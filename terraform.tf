provider "azurerm" {
  version = "~> 2"
  features {}
}

# https://www.terraform.io/docs/backends/types/azurerm.html

terraform {
  required_version = ">= 0.12"
  backend "azurerm" {
    resource_group_name  = "Terraform-RG"
    storage_account_name = "tfstatestorage21669"
    container_name       = "tfstate"
    key                  = "terraform.tfstate"
  }
}
