variable "location" {
  description = "Azure resources location"
  default     = "westeurope"
}
