output "terraform_rg_group" {
  description = "The name of existing RG for Terraform"
  value       = data.azurerm_resource_group.demo
}
