#!/usr/bin/env bash
set -eo pipefail
set -a
. ./init-vars-list.txt
set +a

printf "\n > Setting up remote state backend for Terraform.\n"
printf "\n > Creating Terraform resource group...\n\n"
az group create -n "$TF_RESOURCE_GROUP_NAME" -l "$LOCATION" --tags "Name=Terraform"

printf "\n > Creating storage account for Terraform...\n\n"
az storage account create -g "$TF_RESOURCE_GROUP_NAME" -l "$LOCATION" \
  --name "$TF_STATE_STORAGE_ACCOUNT_NAME" \
  --sku Standard_LRS \
  --encryption-services blob \
  --tags "Name=Terraform"

ACCOUNT_KEY=$(az storage account keys list --resource-group "$TF_RESOURCE_GROUP_NAME" --account-name "$TF_STATE_STORAGE_ACCOUNT_NAME" --query [0].value -o tsv)

printf "\n > Creating storage container for Terraform state files...\n\n"
az storage container create --name "$TF_STATE_STORAGE_CONTAINER_NAME" --account-name "$TF_STATE_STORAGE_ACCOUNT_NAME" --account-key "$ACCOUNT_KEY"

printf "\n > Creating key vault for Terraform...\n\n"
az keyvault create -g "$TF_RESOURCE_GROUP_NAME" -l "$LOCATION" --name "$TF_KEYVAULT_NAME" --enable-soft-delete "true" --tags "Name=Terraform"

printf "\n > Creating Terraform state storage key...\n\n"
az keyvault secret set --name "$SECRET_NAME" --value "$ACCOUNT_KEY" --vault-name "$TF_KEYVAULT_NAME"

printf "\n > Done! \n\n"

read -r -p "Do you want to run Terraform init script for initial setup? (y/n)? " tfinit
case ${tfinit:0:1} in
    y|Y )
        bash ./init-terraform.sh
    ;;
    * )
        printf "\n > OK then, but it is recommended step for the first time setup, so don't forget to run the script. \n\n"
    ;;
esac
